{ stdenv, unzip, vsphere-plugin-zip }:
stdenv.mkDerivation {
  pname = "kubectl-vsphere";
  version = "1.0";
  src = vsphere-plugin-zip;
  phases = [ "buildPhase" ];
  buildPhase = ''
    mkdir -pv $out
    cd $out
    ${unzip}/bin/unzip $src
    rm bin/kubectl
    patchelf --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" bin/kubectl-vsphere
  '';
}
