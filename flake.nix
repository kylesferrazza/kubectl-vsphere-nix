{
  description = "kubectl-vsphere-nix";

  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    defaultPackage.x86_64-linux = pkgs.callPackage ./pkg.nix {
      vsphere-plugin-zip = pkgs.requireFile {
        name = "vsphere-plugin.zip";
        url = "https://kube.lab.nuccdc.club/wcp/plugin/linux-amd64/vsphere-plugin.zip";
        sha256 = "1m44myyw549ajbpz776rkgg5iwdicd0756gcybczsgc4jf1zhlda";
      };
    };
  };
}
